﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    [ServiceContract(CallbackContract = typeof(IZwrotny))]
    public interface ICalculator
    {
        [OperationContract(IsOneWay = true)]
        void Add(int a, int b);
    }

    public class Calculator : ICalculator
    {
        public void Add(int a, int b)
        {
            var zwr = OperationContext.Current.GetCallbackChannel<IZwrotny>();
            zwr.UstawProcent(0);
            System.Threading.Thread.Sleep(3000);

            zwr.UstawProcent(50);
            System.Threading.Thread.Sleep(3000);

            zwr.UstawProcent(80);
            System.Threading.Thread.Sleep(3000);

            zwr.UstawProcent(100);
        }
    }

    interface IZwrotny
    {
        [OperationContract(IsOneWay = true)]
        void UstawProcent(int a);
    }


    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(typeof(Calculator));

            host.AddServiceEndpoint(typeof(ICalculator),
                new NetNamedPipeBinding(),
                "net.pipe://localhost/wcfpipe");

            var b = host.Description.Behaviors.Find<ServiceMetadataBehavior>();
            if (b == null) b = new ServiceMetadataBehavior();
            host.Description.Behaviors.Add(b);

            host.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName,
                MetadataExchangeBindings.CreateMexNamedPipeBinding(),
                "net.pipe://localhost/metadane");

            host.Open();
            Console.ReadKey();
            host.Close();
        }
    }
}
