﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Handler : Calculator.ICalculatorCallback
    {
        public void UstawProcent(int a)
        {
            Console.WriteLine(a);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var client = new Calculator.CalculatorClient(new InstanceContext(new Handler()));
            // korzystamy z usługi
            client.Add(1, 2);

            while (true)
            {

            }
        }
    }
}
